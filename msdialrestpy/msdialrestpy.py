#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import logging
import os
import requests
from requests_toolbelt.multipart import encoder
import tqdm
import zipfile

#
# Configure logging
#
logger = logging.getLogger('MSDialRest')
logger.setLevel(logging.INFO)


#
# Define REST API location
#
REST_URL = 'http://128.120.143.101'


#
# Helper methods
#

def zipdir(zip_filename, path, base_dir):
    """
    Compresses a directory into a zip archive given 
        zip_filename: output archive file to create
        path: a directory path to archive
        path_prefix: path prefix that should be excluded from the directory structure in the zip file
    """

    logger.info('Archiving %s' % zip_filename)

    with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED, allowZip64 = True) as zf:
        for root, dirs, files in os.walk(path):
            for f in files:
                file_path = os.path.join(root, f)
                arcname = base_dir + file_path.split(base_dir)[-1] if base_dir in file_path else file_path
                logger.info('\t%s' % arcname)

                zf.write(file_path, arcname = arcname)


class TqdmRequests(tqdm.tqdm):
    """
    Simple class to interface the requests_toolbelt library for upload
    monitoring and tqdm for progress bar generation
    """

    def update_to(self, monitor):
        self.update(monitor.bytes_read - self.n)



class MSDialRestProcessor(object):

    def __init__(self, path):
        self.path = path


    def process_data(self):
        """
        Run MSDial processing workflow
        """

        logger.info('Processing %s' % self.path)

        response = self.upload_data()
        
        if '/conversion/' in response['link']:
            response = self.convert_to_abf(response)

        response = self.schedule_deconvolution(response)
        self.download_response = self.get_deconvolution_status(response)

        self.save_data()


    def download_data(self):
        """"""

        if hasattr(self, 'download_response'):
            logger.info('Downloading results...')
            return requests.get(self.download_response['link']).text
        else:
            logger.warn('No results found!')
            return None

    def save_data(self):
        if hasattr(self, 'download_response'):
            results = self.download_data()
            output_file = self.path.rsplit('.', 1)[0] +'.msdial'

            logger.info('Saving results to %s' % output_file)

            with open(output_file, 'w') as fout:
                print(results, file = fout)

            return output_file
        else:
            logger.warn('No results found!')
            return None


    def upload_data(self):
        # Handle a .d "file" by zipping the directory
        if os.path.isdir(self.path) and os.path.basename(os.path.normpath(self.path)).lower().endswith('.d'):
            logger.info('Zipping .d directory %s...' % self.path)

            base_dir = os.path.basename(os.path.normpath(self.path))
            file_path = self.path.rsplit('.', 1)[0] +'.zip'
            zipdir(file_path, self.path, base_dir)
        elif os.path.isfile(self.path):
            file_path = self.path
        else:
            raise RuntimeError('Invalid path %s' % self.path)

        # Submit POST file upload request
        logger.info('Uploading %s...' % file_path)

        file_size = os.path.getsize(file_path)
        file_name = file_path.split('/')[-1]
        upload_progress = lambda monitor: logger.info(monitor.bytes_read)

        with open(file_path, 'rb') as f, TqdmRequests(total = file_size, unit = 'b', unit_scale = True, miniters = 1) as t:
            e = encoder.MultipartEncoder(fields = {'file': (file_name, f)})
            m = encoder.MultipartEncoderMonitor(e, t.update_to)
            r = requests.post(REST_URL +'/rest/upload', data = m, headers = {'Content-Type': m.content_type})

        # Return JSON object if possible, otherwise raise a runtime exception
        try:
            response = r.json()
            logger.info('Created job ID %s' % response['link'].split('/')[-1])
            return response
        except Exception as e:
            raise RuntimeError('Upload failed with status code %d, response: "%s"' % (r.status_code, r.text))


    def convert_to_abf(self, upload_response):
        logger.info('Converting uploaded data to ABF format...')

        # Schedule conversion
        r = requests.get(upload_response['link'])

        # Return JSON object if possible, otherwise raise a runtime exception
        try:
            response = r.json()

            # Throw runtime excption if an error message is encountered
            if response['error']:
                raise RuntimeError('\tConverstion scheduling failed with status code %d, response: "%s"' % (r.status_code, response['error']))
            else:
                logger.info('\t%s' % response['message'])
                return response
        
        except Exception as e:
            raise RuntimeError('\tConversion failed with status code %d, response: "%s"' % (r.status_code, r.text))


    def schedule_deconvolution(self, response):
        logger.info('Scheduling deconvolution of ABF data...')

        # Schedule deconvolution
        r = requests.get(response['link'])

        # Return JSON object if possible, otherwise raise a runtime exception
        try:
            response = r.json()

            # Throw runtime excption if an error message is encountered
            if response['error']:
                raise RuntimeError('\tDeconvolution scheduling failed with status code %d, response: "%s"' % (r.status_code, response['error']))
            else:
                if response['message']:
                    logger.info('\t%s' % response['message'])

                return response
        
        except Exception as e:
            raise RuntimeError('\tDeconvolution scheduling failed with status code %d, response: "%s"' % (r.status_code, r.text))


    def get_deconvolution_status(self, response):
        logger.info('Retrieving deconvolution status...')

        # Get deconvolution status
        r = requests.get(response['link'])

        # Return JSON object if possible, otherwise raise a runtime exception
        try:
            response = r.json()

            # Throw runtime excption if an error message is encountered
            if response['error']:
                raise RuntimeError('\tDeconvolution status failed with status code %d, response: "%s"' % (r.status_code, response['error']))
            else:
                if response['message']:
                    logger.info('\t%s' % response['message'])

                self.download_response = response
                logger.info('\tResult file is available for download at %s' % response['link'])

                return response
        
        except Exception as e:
            raise RuntimeError('\tDeconvolution status failed with status code %d, response: "%s"' % (r.status_code, r.text))

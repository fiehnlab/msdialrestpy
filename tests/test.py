#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import logging
import os
import sys
import unittest

try:
    from msdialrestpy import MSDialRestProcessor
except:
    # Try adding the local msdialrestpy package to the path if running from
    # the bin directory without installing
    sys.path.append('../')
    
    from msdialrestpy import MSDialRestProcessor


class TestMSDialRest(unittest.TestCase):
    def test_d_upload(self):
        input_filename = 'data/testA.d'
        zip_filename = input_filename.rsplit('.', 1)[0] +'.zip'
        output_filename = input_filename.rsplit('.', 1)[0] +'.msdial'

        # Test processing
        processor = MSDialRestProcessor(input_filename)
        processor.process_data()
        results = processor.download_data()

        assert(os.path.exists(zip_filename))

        assert('Name\tScanAtLeft' in results)
        assert(len(results.splitlines()) == 12)

        assert(os.path.exists(output_filename))

    def test_abf_upload(self):
        input_filename = 'data/testB.abf'
        output_filename = input_filename.rsplit('.', 1)[0] +'.msdial'

        # Test processing
        processor = MSDialRestProcessor(input_filename)
        processor.process_data()
        results = processor.download_data()

        assert('Name\tScanAtLeft' in results)
        assert(len(results.splitlines()) == 679)

        assert(os.path.exists(output_filename))


if __name__ == '__main__':
    # Configure logging
    logging.basicConfig(format = '%(asctime)s [%(levelname)s] %(name)s: %(message)s')

    # Run tests
    unittest.main(verbosity = 2)
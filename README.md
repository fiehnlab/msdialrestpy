# MSDialRestPy #

MSDialRestPy is a Python client library that supports Python 2.7.x and 3.x. It combines the upload, conversion, scheduling and download process to expedite or automate the data processing workflow.


### Usage ###

```
#!python

from msdialrestpy import MSDialRestProcessor

processor = MSDialRestProcessor('/path/to/raw/data.d')
processor.process_data()
```

will generate `/path/to/raw/data.msdial` containing the deconvolution results.  Internally, this command runs `upload_data()`, `convert_to_abf()`, `schedule_deconvolution()`, `get_deconvolution_status()` and `save_data()`.

The results table can additionally be retrieved as a string using

```
#!python

processor.download_data()
```

### Command-line Tool ###

MSDialRestPy includes a command-line tool to execute jobs using a  runner script to specifying the paths raw or converted data files.  For example,

```
#!bash

python bin/msdialrest_runner.py /path/to/raw/data.d /path/to/file.abf
```

will generate `/path/to/raw/data.msdial` and `/path/to/file.msdial` containing the deconvolution results.
#!/usr/bin/env python

from setuptools import setup


setup(name = 'msdialrestpy',
      version = '1.0',
      description = 'Python client library for the MSDial Rest API',
      author = 'Sajjan Singh Mehta',
      author_email = 'sajjan.s.mehta@gmail.com',
      url = 'https://bitbucket.org/fiehnlab/msdialrestpy',
      packages = ['msdialrestpy'],
      test_suite = 'tests'
)

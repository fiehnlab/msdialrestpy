#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import argparse
import logging
import sys
import traceback

try:
    from msdialrestpy import MSDialRestProcessor
except:
    # Try adding the local msdialrestpy package to the path if running from
    # the bin directory without installing
    sys.path.append('./')
    sys.path.append('../')
    
    from msdialrestpy import MSDialRestProcessor


if __name__ == '__main__':
    # Define command-line arguments
    parser = argparse.ArgumentParser(description = '')
    parser.add_argument('filenames', nargs = '+', help = 'raw data files to process (supports .abf and .d')
    args = parser.parse_args()

    # Configure logging
    logging.basicConfig(format = '%(asctime)s [%(levelname)s] %(name)s: %(message)s')

    logger = logging.getLogger('MSDialRestRunner')
    logger.setLevel(logging.INFO)

    # Process each file
    for f in args.filenames:
        try:
            processor = MSDialRestProcessor(f)
            processor.process_data()
        except Exception as e:
            logger.error('Error encountered when processing %s' % f)
            logger.error(e)
            traceback.print_exception(*sys.exc_info())

